package main

import (
	"bufio"
	"fmt"
	"os"
	"time"
)

func main() {

	polarConfig := NewPolarConfig()

	polar, err := NewPolarAPI(polarConfig)

	if err != nil {
		fmt.Println(err.Error())
		return
	}
	startDate := time.Now().Add(-(time.Hour * 24 * 360))
	list, err := polar.ListWorkouts(startDate, time.Now())

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	totalDistance := 0.0

	for _, workout := range list {
		err := polar.SaveWorkoutTCX(workout)
		if err == nil {
			duration,_ := time.ParseDuration(fmt.Sprintf("%dms", workout.Duration))

			totalDistance += workout.Distance
			date := workout.Datetime.Format("02.01.2006 15:04")
			dist := fmt.Sprintf("%.2f", workout.Distance / 1000)
			dur := fmt.Sprintf("%.0f", duration.Minutes())

			line := fmt.Sprintf("%s %6s km %3s min", date, dist, dur)
			fmt.Println(line)
		}
	}

	fmt.Println(fmt.Sprintf("\ntotal distance: %.2f km ", totalDistance / 1000))

	fmt.Println("\n\n\nPress enter to exit")
	waitForKey()
}

func waitForKey() {
	bufio.NewReader(os.Stdin).ReadLine()
}
