package main

import "time"

type PolarWorkout struct {
	Title             string    `json:"title"`
	Type              string    `json:"type"`
	ListItemID        int       `json:"listItemId"`
	HasTrainingTarget bool      `json:"hasTrainingTarget"`
	Timestamp         int64     `json:"timestamp"`
	Start             int       `json:"start"`
	End               int       `json:"end"`
	AllDay            bool      `json:"allDay"`
	IconURL           string    `json:"iconUrl"`
	URL               string    `json:"url"`
	BorderColor       string    `json:"borderColor"`
	TextColor         string    `json:"textColor"`
	Datetime          time.Time `json:"datetime"`
	EventType         string    `json:"eventType"`
	Duration          int       `json:"duration"`
	Distance          float64   `json:"distance"`
	Calories          int       `json:"calories"`
}
