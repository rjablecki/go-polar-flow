package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/user"
	"strings"
)

const CONFIG_FILE = "go-polar.json"

type PolarConfig struct {
	Email    string
	Password string
}

func NewPolarConfig() *PolarConfig {

	conf := &PolarConfig{}
	usr, err := user.Current()
	if err != nil {
		fmt.Println(err.Error())
		return nil
	}

	filePath := usr.HomeDir + string(os.PathSeparator) + CONFIG_FILE
	_, err = os.Open(filePath)

	if err == nil {
		content, _ := ioutil.ReadFile(filePath)
		json.Unmarshal(content, conf)
	} else {
		email, password := conf.update()
		conf.Email = email
		conf.Password = password
		newConfContent, _ := json.Marshal(conf)
		fmt.Println("new config ", string(newConfContent))
		ioutil.WriteFile(filePath, newConfContent, 0777)
		conf = NewPolarConfig()
	}

	return conf
}

func (conf *PolarConfig) update() (string, string) {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Polar email: ")
	email, _ := reader.ReadString('\n')

	fmt.Print("Polar password: ")
	password, _ := reader.ReadString('\n')

	email = strings.TrimSpace(email)
	password = strings.TrimSpace(password)

	return email, password
}
