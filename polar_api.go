package main

import (
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"io/ioutil"
	"os"
	"strings"
	"time"
)

const (
	POLAR_FLOW_URL_LOGIN    = "https://flow.polar.com/login"
	POLAR_FLOW_URL_WORKOUTS = "https://flow.polar.com/training/getCalendarEvents?start=%s&end=%s"
	POLAR_FLOW_URL_WORKOUT  = "https://flow.polar.com/training/analysis/%d/export/%s/false"
	TRAINING_DIR            = "trainings/"
)

type PolarAPI struct {
	username   string
	password   string
	httpClient *ClientHTTP
}

func NewPolarAPI(conf *PolarConfig) (*PolarAPI, error) {
	api := &PolarAPI{}
	api.httpClient = NewClientHTTP()
	api.username = conf.Email
	api.password = conf.Password
	err := api.login()

	return api, err
}

func (api *PolarAPI) login() error {
	params := map[string]string{}
	params["email"] = api.username
	params["password"] = api.password

	response, err := api.httpClient.post(POLAR_FLOW_URL_LOGIN, params)

	if err != nil {
		return err
	}

	if strings.Contains(response, "Your email or password") {
		return errors.New("Your email or password is incorrect")
	}

	return nil
}

func (api *PolarAPI) ListWorkouts(startDate, endDate time.Time) ([]PolarWorkout, error) {

	u := fmt.Sprintf(POLAR_FLOW_URL_WORKOUTS, startDate.Format("02.01.2006"), endDate.Format("02.01.2006"))
	responseJSON, err := api.httpClient.get(u, nil)

	if err != nil {
		return nil, err
	}

	list := []PolarWorkout{}
	err = json.Unmarshal([]byte(responseJSON), &list)

	if err != nil {
		return nil, err
	}

	return list, nil
}

func (api *PolarAPI) SaveWorkoutTCX(workout PolarWorkout) error {
	fileName := TRAINING_DIR + workout.Datetime.Format("2006-01-02_15-04-05.tcx")
	os.MkdirAll(TRAINING_DIR, 0777)
	_, err := os.Open(fileName)
	if err != nil {
		content, _ := api.FetchWorkoutTCX(workout.ListItemID)
		err = ioutil.WriteFile(fileName, []byte(content), 0777)
		return err
	}
	return nil
}

func (api *PolarAPI) FetchWorkoutTCX(idWorkout int) (string, error) {
	workoutURL := fmt.Sprintf(POLAR_FLOW_URL_WORKOUT, idWorkout, "tcx")
	response, err := api.httpClient.get(workoutURL, nil)

	return response, err
}
