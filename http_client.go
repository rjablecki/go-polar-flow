package main

import (
	"bytes"
	"golang.org/x/net/publicsuffix"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/url"
)

type ClientHTTP struct {
	cookieJar *cookiejar.Jar
	client    *http.Client
}

func NewClientHTTP() *ClientHTTP {
	c := &ClientHTTP{}
	options := cookiejar.Options{
		PublicSuffixList: publicsuffix.List,
	}
	jar, err := cookiejar.New(&options)
	if err != nil {
		log.Fatal(err)
	}
	c.cookieJar = jar
	cl := &http.Client{
		Jar: jar,
	}
	c.client = cl
	return c
}

func (c *ClientHTTP) post(postURL string, params map[string]string) (string, error) {

	values := make(url.Values)
	for name, val := range params {
		values.Add(name, val)
	}

	request, err := http.NewRequest("POST", postURL, bytes.NewBufferString(values.Encode()))
	if err != nil {
		return "", err
	}

	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	response, err := c.client.Do(request)
	if err != nil {
		return "", err
	}
	responseBody, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", err
	}
	response.Body.Close()

	return string(responseBody), nil
}

func (c *ClientHTTP) get(url string, params map[string]string) (string, error) {

	response, err := c.client.Get(url)
	if err != nil {
		return "", err
	}

	responseBody, _ := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", err
	}
	response.Body.Close()

	return string(responseBody), nil
}
